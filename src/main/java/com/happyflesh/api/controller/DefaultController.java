package com.happyflesh.api.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class DefaultController {

    @GetMapping
    public String testApi(){
        return "Ok";
    }

    @GetMapping("/{name}")
    public String getNameApi(@PathVariable String name){
        return "Hi " + name;
    }
}
